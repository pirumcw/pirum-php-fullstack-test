<?php
# DB connection params, please use this structure for connecting. 
# Please do not add any code here and do not send back this file with your settings.
class DBConf {
  const USER = '';
  const PASS = '';
  const HOST = '';
  const DB   = '';
}
