## Pirum PHP/Front-End Interview Test

Please read the [pdf file](https://bitbucket.org/pirumcw/pirum-php-fullstack-test/raw/2f4afd758b4b0ca46699f9017a181e3a362c53e5/Pirum%20PHP%20Front-end%20test.pdf) 
for the full description with screenshots

#### Introduction
At Pirum we work with different securities (tradeable financial instruments) which are identified by
multiple securities code standards like ISIN, CUSIP, SEDOL. (There are actually more than this three,
but in the test we just use these) The same security can be identified by the different schemes, like:

    Description                                       ISIN                  CUSIP              SEDOL
    US TREASURY NOTE 0.375 15 JAN 2016 RG             US912828UG38          912828UG3          B9CJZS0
*(It not necessary that all the different types exist for the same security)*

Since we want to let our users easily handle the different securities on out site, we provide a user
friendly search-as-you-type securities selector (similar like the google instant search) in our form.

In this test your task is to make a simple implementation of such a search.


#### Specification  

* The user should be able to search in the ISIN field using an incremental search. The
  application will match this against ALL value fields from the database fields (ISIN,
  Description, CUSIP and SEDOL), refining the search as the user types.  
  ![Screen Shot 2017-04-03 at 22.52.23.png](https://bitbucket.org/repo/5qxryxM/images/204699878-Screen%20Shot%202017-04-03%20at%2022.52.23.png)

* During the incremental search, matching records should be shown in a dropdown selector
  for the user to choose from, while still allowing them to type more characters to narrow
  down the search, something like the screenshot below. The dropdown should be one line
  per ISIN, showing the possibly multiple related matches on the same line.  
  ![Screen Shot 2017-04-03 at 22.52.42.png](https://bitbucket.org/repo/5qxryxM/images/2656665816-Screen%20Shot%202017-04-03%20at%2022.52.42.png)

* Once the user has selected a matching record, the ISIN and Description fields should be
  populated with the matching values.  

* In the test files there is a MYSQL database dump file which contains some example security
  data “security_master_test.sql” and it is preferred if you could use MySQL on the PHP side
  to execute the search. If you do not have access to MySQL, there is a csv text file
  “security_master_test.csv” and you could parse that file to provide the data to search
  against. (Note: If you implement the search based on the csv file, please write down the SQL
  queries you would use against a MYSQL database)  


* The database is structured so that the different type of codes are connected through 
the sec_id column.  
  ![Screen Shot 2017-04-03 at 22.53.04.png](https://bitbucket.org/repo/5qxryxM/images/2760098199-Screen%20Shot%202017-04-03%20at%2022.53.04.png)


#### The following files are included:

|  File                          |  Description                                   |
|  ----------------------------  |  --------------------------------------------  |
|  dbconf.php                    |  DB conf (don’t send back with your settings)  |
|  index.html                    |  Simple html form                              | 
|  jquery-1.11.3.min.js          |  jQuery                                        |
|  pirumtest.css                 |  CSS file                                      |
|  pirumtest.js                  |  JS file                                       |
|  search.php                    |  Ajax endpoint for the search                  |
|  security_master_test.csv      |  CSV data dump, 1000 example records           |
|  security_master_test.sql      |  SQL data dump, 1000 example records           |
|  Pirum PHP Front-end test.pdf  |  This file                                     |


#### Expected solution

1. We expect a working implementation of the search-as-you-type form. Please put some 
   comments into your code to describe what approach and algorithm you’ve chosen and why.  
2. Please use JQuery in your javascript code, a simple jquery.min.js is included in the test files.
   You can use any jquery plugin you want, the test is not about writing low level ajax code, but
   using the appropriate tool to solve a problem efficiently. (although if you want, you’re free
   to do the test without any plugins, just using simple jquery)  
3. Please note, the real database contains 200,000+ records, so your design should take into
   account the potential performance problem in hitting the table with excessive amount of
   queries on each keystroke. Please provide some thoughts on what strategies you would use
   to handle these performance concerns. Performance and efficiency will be some of the main
   factors we will look at when reviewing the results of the test.